<?php

namespace TuteurBundle\Controller;

use AppBundle\Entity\Classe;
use AppBundle\Entity\Groupe;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Groupe controller.
 *
 * @Route("groupe")
 */
class GroupeController extends Controller
{
    /**
     * Lists all groupe entities.
     *
     * @Route("/", name="groupe_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $groupe = new Groupe();
        $form = $this->createForm('TuteurBundle\Form\GroupeType', $groupe);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $groupe->setTuteur($this->getUser());
            $em->persist($groupe);
            $em->flush();

            return $this->redirectToRoute('groupe_index');
        }
        $groupes = $this->getUser()->getTuteuredGroups();
        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $groupes, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );
        return $this->render('TuteurBundle:groupe:index.html.twig', array(
            'groupes' => $pagination,
            'groupe' => $groupe,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new groupe entity.
     *
     * @Route("/new", name="groupe_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $groupe = new Groupe();
        $form = $this->createForm('TuteurBundle\Form\GroupeType', $groupe, array(
            'classes' => $this->getUser()->getTuteurClasses()
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($groupe);
            $em->flush();

            return $this->redirectToRoute('groupe_show', array('id' => $groupe->getId()));
        }

        return $this->render('@Tuteur/groupe/new.html.twig', array(
            'groupe' => $groupe,
            'form' => $form->createView(),
        ));
    }
    /**
     * Creates a new groupe entity.
     *
     * @Route("/addMembers/{groupe}", name="groupe_add_members")
     * @Method({"GET", "POST"})
     */
    public function addMemberAction(Request $request,Groupe $groupe)
    {
        $list=array();
        $em=$this->getDoctrine()->getManager();
        if($request->isMethod('post'))
        {
            $list=$request->get('Etudiants');
            if (empty($list)){
                return $this->redirectToRoute('groupe_show',array('id'=>$groupe->getId()));
            }else{
                foreach($list as $v => $value) {
                    $emp=$em->getRepository('AppBundle:User')->find($value);
                    $emp->setGroup($groupe);
                    $em->persist($emp);
                    $em->flush();
                }
                return $this->redirectToRoute('groupe_show',array('id'=>$groupe->getId()));
            }


        }
    }

    /**
     * Finds and displays a groupe entity.
     *
     * @Route("/{id}", name="groupe_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Groupe $groupe)
    {
        $em = $this->getDoctrine()->getManager();
        $etudiant_nonaffecter = $em->getRepository('AppBundle:User')->findByStudentswithClass('ROLE_ETUDIANT',$groupe->getClasse());

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $etudiant_nonaffecter, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );
        return $this->render('@Tuteur/groupe/show.html.twig', array(
            'groupe' => $groupe,
            'etudiants_not_affected'=>$pagination,
            'etudiants'=>$groupe->getEtudiants()
        ));
    }

    /**
     * Displays a form to edit an existing groupe entity.
     *
     * @Route("/{id}/edit", name="groupe_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Groupe $groupe)
    {
        $deleteForm = $this->createDeleteForm($groupe);
        $editForm = $this->createForm('TuteurBundle\Form\GroupeType', $groupe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('groupe_edit', array('id' => $groupe->getId()));
        }

        return $this->render('@Tuteur/groupe/edit.html.twig', array(
            'groupe' => $groupe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a groupe entity.
     *
     * @Route("/delete/{id}", name="groupe_delete_get")
     * @Method("GET")
     */
    public function SuprimerAction(Request $request, Groupe $id)
    {

        $em = $this->getDoctrine()->getManager();
        $specialite = $em->getRepository('AppBundle:Groupe')->find($id);
        $em->remove($specialite);
        $em->flush();

        return $this->redirectToRoute('groupe_index');
    }
    /**
     * Deletes a groupe member entity.
     *
     * @Route("/delete/{id}/{etudiant}", name="groupe_delete_member")
     * @Method("GET")
     */
    public function SuprimerMembreAction(Request $request, Groupe $id, User $etudiant)
    {

        $em = $this->getDoctrine()->getManager();
        $etudiant->setGroup(null);
        $em->persist($etudiant);
        $em->flush();

        return $this->redirectToRoute('groupe_show',array('id'=>$id->getId()));
    }
    /**
     * Deletes a groupe entity.
     *
     * @Route("/{id}", name="groupe_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Groupe $groupe)
    {
        $form = $this->createDeleteForm($groupe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($groupe);
            $em->flush();
        }

        return $this->redirectToRoute('groupe_index');
    }

    /**
     * Creates a form to delete a groupe entity.
     *
     * @param Groupe $groupe The groupe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Groupe $groupe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('groupe_delete', array('id' => $groupe->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
