<?php

namespace TuteurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TacheType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('type',ChoiceType::class,[
                'label' => 'Type',
                'choices' => array(
                    'Crud' => 'Crud',
                    'Bundle Externe' => 'Bundle Externe',
                    'Metier' => 'Metier'
                ),
            ])
            ->add('date_deb',DateType::class,
                [
                    'required' => true,
                    'widget' => 'single_text'
                ])
            ->add('date_fin',DateType::class,
                [
                    'required' => true,
                    'widget' => 'single_text'
                ])
            ->add('etudiant')
            ->add('description');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Tache'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_tache';
    }


}
