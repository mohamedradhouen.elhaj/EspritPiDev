<?php

namespace TuteurBundle\Form;

use AppBundle\Entity\Classe;
use AppBundle\Repository\ClasseRepository;
use AppBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('sujet',EntityType::class,[
                'class' => 'AppBundle\Entity\Sujet',
                'placeholder' => 'Selectionnez un Sujet'
            ])
            ->add('classe',EntityType::class,[
                'class' => 'AppBundle\Entity\Classe',
                'placeholder' => 'Selectionnez un Classe',
                'query_builder' => function(ClasseRepository $repo){
                    return $repo->findClasse();
                }
            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Groupe'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_groupe';
    }


}
