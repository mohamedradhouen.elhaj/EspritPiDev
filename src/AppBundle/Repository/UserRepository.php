<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 7/26/18
 * Time: 1:18 AM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Classe;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findByRole($role) {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u')
            ->from('AppBundle:User', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->andWhere('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');

        return $user = $qb->getQuery()->getResult();

    }
    public function findEnabledUsersByRole($role) {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u')
            ->from('AppBundle:User', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->andWhere('u.roles LIKE :roles')
            ->andWhere('u.enabled = 1')
            ->setParameter('roles', '%"' . $role . '"%');

        return $user = $qb->getQuery()->getResult();

    }
    public function findByStudentswithClass($role,Classe $classe) {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u')
            ->from('AppBundle:User', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->andWhere('u.roles LIKE :roles')
            ->andWhere('u.classe = :classe')
            ->andWhere('u.group is null')
            ->setParameters([
                'roles' => '%"' . $role . '"%',
                'classe' => $classe->getId()
            ]);

        return $user = $qb->getQuery()->getResult();

    }
    public function findByStudentswithoutClasses($role) {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u')
            ->from('AppBundle:User', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->andWhere('u.roles LIKE :roles')
            ->andWhere('u.classe is null ')
            ->setParameter('roles', '%"' . $role . '"%');

        return $user = $qb->getQuery()->getResult();

    }

    public function findClasse() {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u')
            ->from('AppBundle:Classe', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->innerJoin('u.tuteurs','c');

        return $qb;

    }
    public function findByRoleQueryBuilder($role) {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u' )
            ->from('AppBundle:User', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->andWhere('u.roles LIKE :roles')
            ->andWhere('u.enabled = 1')
            ->setParameter('roles', '%"' . $role . '"%');
        return $qb;
    }
    public function findByRoleQuery($role) {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u.id','u.firstName', 'u.lastName','u.email','u.username','u.enabled' )
            ->from('AppBundle:User', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->andWhere('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');
        return $qb->getQuery();
    }
    public function findallUserseQuery() {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u.id','u.firstName', 'u.lastName','u.email','u.username','u.enabled' ,'u.roles')
            ->from('AppBundle:User', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->where("u.roles LIKE  '%ROLE_VALIDATEUR%'")
            ->orWhere("u.roles LIKE  '%ROLE_TUTEUR%'")
            ->orWhere("u.roles LIKE '%ROLE_ETUDIANT%'")
            ->addOrderBy('u.firstName');
        return $qb->getQuery();
    }
}