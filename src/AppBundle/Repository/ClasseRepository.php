<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/23/18
 * Time: 4:05 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ClasseRepository extends EntityRepository
{
    public function findWithGroupes() {
        // Entity manager
        $em= $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('c')
            ->from('AppBundle:Classe', 'c') // Change this to the name of your bundle and the name of your mapped user Entity
            ->innerJoin('c.groupes',"g");

        return $qb;

    }
    public function findClasse() {
        // Entity manager
        $em= $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u')
            ->from('AppBundle:Classe', 'u') // Change this to the name of your bundle and the name of your mapped user Entity
            ->innerJoin('u.tuteurs','c');

        return $qb;

    }
}