<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/23/18
 * Time: 4:44 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class GroupeRepository extends EntityRepository
{
    public function findWithSujet($classe) {
        // Entity manager
        $em= $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('g')
            ->from('AppBundle:Groupe', 'g') // Change this to the name of your bundle and the name of your mapped user Entity
            ->join('g.sujet',"s");

        return $qb;

    }
}