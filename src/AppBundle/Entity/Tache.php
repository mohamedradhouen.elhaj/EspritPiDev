<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/2/18
 * Time: 6:40 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Tache
 * @package AppBundle\Entity
 * @ORM\Entity()
 */
class Tache
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var
     * @ORM\Column(type="text",length=999)
     */
    private $description;
    /**
     * @var
     * @ORM\Column(type="string",length=11)
     */
    private $type;
    /**
     * @var
     * @ORM\Column(type="datetime")
     * @Assert\Range(
     *      min = "now"
     * )
     */
    private $date_deb;
    /**
     * @var
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     * @Assert\Expression(
     *     "this.getDateFin() > this.getDateDeb()",
     *     message="Date fin doit etre supperieur à la date debut"
     * )
     */
    private $date_fin;

    /**
     * @var
     * @ORM\Column(type="boolean")
     */
    private $etat = false;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",fetch="EAGER",inversedBy="taches")
     * @ORM\JoinColumn(referencedColumnName="id",name="etudiant_id",onDelete="CASCADE")
     */
    private $etudiant;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDateDeb()
    {
        return $this->date_deb;
    }

    /**
     * @param mixed $date_deb
     */
    public function setDateDeb($date_deb)
    {
        $this->date_deb = $date_deb;
    }

    /**
     * @return mixed
     */
    public function getDateFin()
    {
        return $this->date_fin;
    }

    /**
     * @param mixed $date_fin
     */
    public function setDateFin($date_fin)
    {
        $this->date_fin = $date_fin;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * @param mixed $etudiant
     */
    public function setEtudiant($etudiant)
    {
        $this->etudiant = $etudiant;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


}