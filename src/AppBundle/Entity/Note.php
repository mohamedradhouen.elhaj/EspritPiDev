<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/2/18
 * Time: 6:33 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Note
 * @package AppBundle\Entity
 * @ORM\Entity()
 */
class Note
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Sprint")
     * @ORM\JoinColumn(name="sprint_id",referencedColumnName="id")
     */
    private $sprint;
    /**
     * @var
     * @ORM\Column(type="float", length=11)
     */
    private $noteIndividuel;

    /**
     * @var
     * @ORM\Column(type="float", length=11)
     */
    private $noteGroup;
    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User",fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="id",name="validateur_id",onDelete="SET NULL")
     */
    private $validateur;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSprint()
    {
        return $this->sprint;
    }

    /**
     * @param mixed $sprint
     */
    public function setSprint($sprint)
    {
        $this->sprint = $sprint;
    }

    /**
     * @return mixed
     */
    public function getNoteIndividuel()
    {
        return $this->noteIndividuel;
    }

    /**
     * @param mixed $noteIndividuel
     */
    public function setNoteIndividuel($noteIndividuel)
    {
        $this->noteIndividuel = $noteIndividuel;
    }

    /**
     * @return mixed
     */
    public function getNoteGroup()
    {
        return $this->noteGroup;
    }

    /**
     * @param mixed $noteGroup
     */
    public function setNoteGroup($noteGroup)
    {
        $this->noteGroup = $noteGroup;
    }

    /**
     * @return mixed
     */
    public function getValidateur()
    {
        return $this->validateur;
    }

    /**
     * @param mixed $validateur
     */
    public function setValidateur($validateur)
    {
        $this->validateur = $validateur;
    }



}