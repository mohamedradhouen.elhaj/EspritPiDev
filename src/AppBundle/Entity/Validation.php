<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/13/18
 * Time: 1:25 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class Validation
 * @package AppBundle\Entity
 * @ORM\Entity()
 */
class Validation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var
     * @ORM\Column(type="datetime")
     */
    private $date;
    /**
     * @var
     * @ORM\Column(type="string")
     */
    private $type;
    /**
     * @var
     * @Assert\NotBlank()
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User",fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="id",name="validateur_id",onDelete="CASCADE")
     */
    private $validateur;
    /**
     * @var
     * @Assert\NotBlank()
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Groupe",fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="id",name="group_id",onDelete="CASCADE")
     */
    private $groupe;

    /**
     * Many Features have One Product.
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Specialite",fetch="EAGER")
     * @ORM\JoinColumn(name="specialite_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $specialite;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getValidateur()
    {
        return $this->validateur;
    }

    /**
     * @param mixed $validateur
     */
    public function setValidateur($validateur)
    {
        $this->validateur = $validateur;
    }

    /**
     * @return mixed
     */
    public function getGroupe()
    {
        return $this->groupe;
    }

    /**
     * @param mixed $groupe
     */
    public function setGroupe($groupe)
    {
        $this->groupe = $groupe;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * @return mixed
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * @param mixed $specialite
     */
    public function setSpecialite($specialite)
    {
        $this->specialite = $specialite;
    }



}