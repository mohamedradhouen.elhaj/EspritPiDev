<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 7/22/18
 * Time: 3:53 PM
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Classe
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClasseRepository")
 * @ORM\Table(name="Classe")
 */
class Classe
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="string", length=255)
     */
    private $nom;
    /**
     * @var
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $email;
    /**
     * Many Features have One Product.
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Specialite", inversedBy="classes",fetch="EAGER")
     * @ORM\JoinColumn(name="specialite_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $specialite;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="tuteurClasses",fetch="EAGER")
     * @ORM\JoinTable(name="tuteur_class")
     */
    private $tuteurs;
    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="classe",fetch="EAGER")
     */
    private $etudiants;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Groupe", mappedBy="classe",fetch="EAGER")
     */
    private $groupes;

    /**
     * Classe constructor.
     * @param $tuteurs
     */
    public function __construct()
    {
        $this->tuteurs = new ArrayCollection();;
        $this->etudiants = new ArrayCollection();;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @return mixed
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * @param mixed $specialite
     */
    public function setSpecialite($specialite)
    {
        $this->specialite = $specialite;
    }

    /**
     * @return mixed
     */
    public function getTuteurs()
    {
        return $this->tuteurs;
    }

    /**
     * @param mixed $tuteurs
     */
    public function setTuteurs($tuteurs)
    {
        $this->tuteurs = $tuteurs;
    }

    /**
     * @return mixed
     */
    public function getEtudiants()
    {
        return $this->etudiants;
    }

    /**
     * @param mixed $etudiants
     */
    public function setEtudiants($etudiants)
    {
        $this->etudiants = $etudiants;
    }

    /**
     * @return mixed
     */
    public function getGroupes()
    {
        return $this->groupes;
    }

    /**
     * @param mixed $groupes
     */
    public function setGroupes($groupes)
    {
        $this->groupes = $groupes;
    }
    public function addUser(User $user)
    {
        if ($this->tuteurs->contains($user)) {
            return;
        }
        $this->tuteurs->add($user);
        $user->addTutuerClasse($this);
    }

    public function removeUser(User $user)
    {
        if (!$this->tuteurs->contains($user)) {
            return;
        }
        $this->tuteurs->removeElement($user);
        $user->removeTutuerClasse($this);
    }

    public function __toString()
    {
        return $this->nom;
    }


}