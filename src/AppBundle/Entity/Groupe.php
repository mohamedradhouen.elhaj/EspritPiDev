<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 7/22/18
 * Time: 3:57 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Classe
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupeRepository")
 * @ORM\Table(name="Groupe")
 */
class Groupe
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="string", length=255)
     */
    private $nom;
    /**
     * Many Features have One Product.
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Specialite", inversedBy="groupes",fetch="EAGER")
     * @ORM\JoinColumn(name="specialite_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $specialite;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="group")
     */
    private $etudiants;

    /**
     * One Product has Many Features.
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",fetch="EAGER",inversedBy="tuteuredGroups")
     * @ORM\JoinColumn(name="tuteur_id",referencedColumnName="id",onDelete="SET NULL")
     */
    private $tuteur;

    /**
     * Many Features have One Product.
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Classe", inversedBy="groupes",fetch="EAGER")
     * @ORM\JoinColumn(name="classe_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $classe;
    /**
     * @var
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sujet",fetch="EAGER",inversedBy="groups")
     * @ORM\JoinColumn(referencedColumnName="id",name="sujet_id")
     */
    private $sujet;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * @param mixed $specialite
     */
    public function setSpecialite($specialite)
    {
        $this->specialite = $specialite;
    }

    /**
     * @return mixed
     */
    public function getEtudiants()
    {
        return $this->etudiants;
    }

    /**
     * @param mixed $etudiants
     */
    public function setEtudiants($etudiants)
    {
        $this->etudiants = $etudiants;
    }

    /**
     * @return mixed
     */
    public function getTuteur()
    {
        return $this->tuteur;
    }

    /**
     * @param mixed $tuteur
     */
    public function setTuteur($tuteur)
    {
        $this->tuteur = $tuteur;
    }

    /**
     * @return mixed
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    }

    /**
     * @return mixed
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * @param mixed $sujet
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;
    }
    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->nom;
    }


}