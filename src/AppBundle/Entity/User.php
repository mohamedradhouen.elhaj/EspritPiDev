<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 7/21/18
 * Time: 8:58 PM
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\AttributeOverride;
use Doctrine\ORM\Mapping\AttributeOverrides;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="User")
 * @UniqueEntity(
 *     fields={"usernameCanonical"},
 *     errorPath="usernameCanonical",
 *     message="Le nom d'utilisateur est deja utilisé")
 *  @UniqueEntity(
 *     fields={"emailCanonical"},
 *     errorPath="emailCanonical",
 *     message="L'adresse mail est deja utilisé")
 * @AttributeOverrides({
 *     @AttributeOverride(name="emailCanonical",
 *         column=@ORM\Column(
 *             type="string",
 *             length=255,
 *             unique=true
 *         )
 *     ),
 *     @AttributeOverride(name="usernameCanonical",
 *         column=@ORM\Column(
 *              type="string",
 *              unique=true
 *              )
 *      )
 * })
 */


class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    private $firstName;
    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    private $lastName;
    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Classe", inversedBy="etudiants",fetch="EAGER")
     * @ORM\JoinColumn(name="classe_id", referencedColumnName="id",onDelete="SET NULL")
     */
    private $classe;
    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Groupe", inversedBy="etudiants",fetch="EAGER")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id",onDelete="SET NULL")
     */
    private $group;
    /**
     * Many Groups have Many Users.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Classe", mappedBy="tuteurs",fetch="EAGER")
     */
    private $tuteurClasses;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Groupe", mappedBy="tuteur",fetch="EAGER")
     */
    private $tuteuredGroups;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Tache", mappedBy="etudiant",fetch="EAGER")
     */
    private $taches;
    /**
     * User constructor.
     * @param $tuteurClasses
     */
    public function __construct()
    {
        parent::__construct();
        $this->tuteurClasses =  new ArrayCollection();;
        $this->tuteuredGroups =  new ArrayCollection();;
        $this->taches =  new ArrayCollection();;
    }


    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getTuteurClasses()
    {
        return $this->tuteurClasses;
    }
    /**
     * @return mixed
     */
    public function getTuteursClasses()
    {
        return $this->tuteurClasses->getValues();
    }

    /**
     * @return mixed
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    }




    public function addTutuerClasse(Classe $classe)
    {
        if ($this->tuteurClasses->contains($classe)) {
            return;
        }
        $this->tuteurClasses->add($classe);
        $classe->addUser($this);
    }
    public function removeTutuerClasse(Classe $classe)
    {
        if (!$this->tuteurClasses->contains($classe)) {
            return;
        }
        $this->tuteurClasses->removeElement($classe);
        $classe->removeUser($this);
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getTuteuredGroups()
    {
        return $this->tuteuredGroups;
    }

    /**
     * @param mixed $tuteuredGroups
     */
    public function setTuteuredGroups($tuteuredGroups)
    {
        $this->tuteuredGroups = $tuteuredGroups;
    }

    /**
     * @return mixed
     */
    public function getTaches()
    {
        return $this->taches;
    }

    /**
     * @param mixed $taches
     */
    public function setTaches($taches)
    {
        $this->taches = $taches;
    }



}