<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/23/18
 * Time: 1:09 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Note
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="Notes")
 */
class Notes
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Note")
     * @ORM\JoinColumn(name="sprint_gl_note",referencedColumnName="id")
     */
    private $sprint_gl_note;
    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Note")
     * @ORM\JoinColumn(name="sprint_web_note",referencedColumnName="id")
     */
    private $sprint_web_note;
    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Note")
     * @ORM\JoinColumn(name="sprint_desktop_note",referencedColumnName="id")
     */
    private $sprint_desktop_note;
    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Note")
     * @ORM\JoinColumn(name="sprint_mobile_note",referencedColumnName="id")
     */
    private $sprint_mobile_note;
    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Note")
     * @ORM\JoinColumn(name="soutenance_finale_note",referencedColumnName="id")
     */
    private $soutenance_finale_note;

    /**
     * @var
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User",fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="id",name="etudiant_id",onDelete="SET NULL")
     */
    private $Etudiant;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSprintGlNote()
    {
        return $this->sprint_gl_note;
    }

    /**
     * @param mixed $sprint_gl_note
     */
    public function setSprintGlNote($sprint_gl_note)
    {
        $this->sprint_gl_note = $sprint_gl_note;
    }

    /**
     * @return mixed
     */
    public function getSprintWebNote()
    {
        return $this->sprint_web_note;
    }

    /**
     * @param mixed $sprint_web_note
     */
    public function setSprintWebNote($sprint_web_note)
    {
        $this->sprint_web_note = $sprint_web_note;
    }

    /**
     * @return mixed
     */
    public function getSprintDesktopNote()
    {
        return $this->sprint_desktop_note;
    }

    /**
     * @param mixed $sprint_desktop_note
     */
    public function setSprintDesktopNote($sprint_desktop_note)
    {
        $this->sprint_desktop_note = $sprint_desktop_note;
    }

    /**
     * @return mixed
     */
    public function getSprintMobileNote()
    {
        return $this->sprint_mobile_note;
    }

    /**
     * @param mixed $sprint_mobile_note
     */
    public function setSprintMobileNote($sprint_mobile_note)
    {
        $this->sprint_mobile_note = $sprint_mobile_note;
    }

    /**
     * @return mixed
     */
    public function getEtudiant()
    {
        return $this->Etudiant;
    }

    /**
     * @param mixed $Etudiant
     */
    public function setEtudiant($Etudiant)
    {
        $this->Etudiant = $Etudiant;
    }

    /**
     * @return mixed
     */
    public function getSoutenanceFinaleNote()
    {
        return $this->soutenance_finale_note;
    }

    /**
     * @param mixed $soutenance_finale_note
     */
    public function setSoutenanceFinaleNote($soutenance_finale_note)
    {
        $this->soutenance_finale_note = $soutenance_finale_note;
    }


}