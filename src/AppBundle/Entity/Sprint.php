<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/2/18
 * Time: 6:42 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Sprint
 * @package AppBundle\Entity
 * @ORM\Entity()
 */
class Sprint
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="string",length=255)
     */
    private $name;
    /**
     * @var
     * @ORM\Column(type="datetime")
     * @Assert\Range(
     *      min = "now"
     * )
     */
    private $date_deb;
    /**
     * @var
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     * @Assert\Expression(
     *     "this.getDateFin() > this.getDateDeb()",
     *     message="Date fin doit etre supperieur à la date debut"
     * )
     */
    private $date_fin;
    /**
     * Many Features have One Product.
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Specialite",fetch="EAGER")
     * @ORM\JoinColumn(name="specialite_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $specialite;

    /**
     * @var
     * @ORM\Column(type="float",length=255)
     */
    private $coefSprint;
    /**
     * @var
     * @ORM\Column(type="float",length=255)
     */
    private $CoefNoteIndiv;
    /**
     * @var
     * @ORM\Column(type="float",length=255)
     */
    private $CoefNoteGroupe;

    /**
     * @var
     * @ORM\Column(type="string", length=4000)
     */
    private $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDateDeb()
    {
        return $this->date_deb;
    }

    /**
     * @param mixed $date_deb
     */
    public function setDateDeb($date_deb)
    {
        $this->date_deb = $date_deb;
    }

    /**
     * @return mixed
     */
    public function getDateFin()
    {
        return $this->date_fin;
    }

    /**
     * @param mixed $date_fin
     */
    public function setDateFin($date_fin)
    {
        $this->date_fin = $date_fin;
    }

    /**
     * @return mixed
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * @param mixed $specialite
     */
    public function setSpecialite($specialite)
    {
        $this->specialite = $specialite;
    }

    /**
     * @return mixed
     */
    public function getCoefSprint()
    {
        return $this->coefSprint;
    }

    /**
     * @param mixed $coefSprint
     */
    public function setCoefSprint($coefSprint)
    {
        $this->coefSprint = $coefSprint;
    }

    /**
     * @return mixed
     */
    public function getCoefNoteIndiv()
    {
        return $this->CoefNoteIndiv;
    }

    /**
     * @param mixed $CoefNoteIndiv
     */
    public function setCoefNoteIndiv($CoefNoteIndiv)
    {
        $this->CoefNoteIndiv = $CoefNoteIndiv;
    }

    /**
     * @return mixed
     */
    public function getCoefNoteGroupe()
    {
        return $this->CoefNoteGroupe;
    }

    /**
     * @param mixed $CoefNoteGroupe
     */
    public function setCoefNoteGroupe($CoefNoteGroupe)
    {
        $this->CoefNoteGroupe = $CoefNoteGroupe;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }





}