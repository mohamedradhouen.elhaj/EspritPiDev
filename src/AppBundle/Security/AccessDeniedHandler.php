<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/8/18
 * Time: 2:04 PM
 */

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $roles = getRoles();
        // Tranform this list in array
        $rolesTab = array_map(function ($role) {
            return $role->getRole();
        }, $roles);

        // If is a admin or super admin we redirect to the backoffice area
        if (in_array('ROLE_SUPER_ADMIN', $rolesTab, true))
            $redirection = new RedirectResponse($this->router->generate('homepage_super_admin'));
        if (in_array('ROLE_ADMIN', $rolesTab, true))
            $redirection = new RedirectResponse($this->router->generate('homepage_super_admin'));
        if (in_array('ROLE_ETUDIANT', $rolesTab, true))
            $redirection = new RedirectResponse($this->router->generate('homepage_etudiant'));
        if (in_array('ROLE_TUTEUR', $rolesTab, true))
            $redirection = new RedirectResponse($this->router->generate('homepage_tuteur'));
        if (in_array('ROLE_VALIDATEUR', $rolesTab, true))
            $redirection = new RedirectResponse($this->router->generate('homepage_validateur'));
        // otherwise, if is a commercial user we redirect to the crm area

        return $redirection;
    }
}