<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser() == null){
            return $this->redirectToRoute("fos_user_security_login");
        }else{
            if ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')){
                return $this->redirectToRoute("homepage_super_admin");
            }
            if ($this->getUser()->hasRole('ROLE_ADMIN')){
                return $this->redirectToRoute("homepage_super_admin");
            }
            if ($this->getUser()->hasRole('ROLE_TUTEUR')){
                return $this->redirectToRoute("homepage_tuteur");
            }
            if ($this->getUser()->hasRole('ROLE_VALIDATEUR')){
                return $this->redirectToRoute("homepage_tuteur");
            }
            if ($this->getUser()->hasRole('ROLE_ETUDIANT')){
                return $this->redirectToRoute("homepage_etudiant");
            }

        }


    }
    /**
     * @Route("/Denied", name="access_denied")
     */
    public function AccesDeniedAction(Request $request)
    {

        return $this->render('exception.html.twig');
    }
}
