<?php
/**
 * Created by PhpStorm.
 * User: medradhouenelhaj
 * Date: 8/5/18
 * Time: 5:16 PM
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppFixtures extends Fixture
{


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstName('Gharbi ');
        $user->setLastName('Sofien ');
        $user->setUsername('Admin');
        $user->setEmail('Sofiene.Gharbi@esprit.tn');
        $user->setPlainPassword('admin');
        $user->addRole('ROLE_SUPER_ADMIN');
        $user->setEnabled(true);
        $manager->persist($user);

        $manager->flush();
    }

}