<?php

namespace SuperAdminBundle\Form;

use AppBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ValidationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateTimeType::class, array(
                'data'=>new \DateTime('now'),
                'widget' => 'single_text',
                'attr' => array(
                    'class'=>'form-control'
                    )
                    )
                )
            ->add('validateur',EntityType::class,[
                'class' => 'AppBundle\Entity\User',
                'query_builder' => function(UserRepository $userRepository){
                    return $userRepository->findByRoleQueryBuilder('ROLE_VALIDATEUR');
                }
            ])
            ->add('type',ChoiceType::class,[
                'label' => 'Type',
                'choices' => array(
                    'Sprint GL' => 'Sprint GL',
                    'Sprint Web' => 'Sprint Web',
                    'Sprint Desktop' => 'Sprint Desktop',
                    'Sprint Mobile' => 'Sprint Mobile',
                    'Soutenance Finale' => 'Soutenance Finale'
                )
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Validation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_validation';
    }


}
