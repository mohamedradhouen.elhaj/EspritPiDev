<?php

namespace SuperAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SprintType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',ChoiceType::class,[
                'label' => 'Nom',
                'choices' => array(
                    'Sprint GL' => 'Sprint GL',
                    'Sprint Web' => 'Sprint Web',
                    'Sprint Desktop' => 'Sprint Desktop',
                    'Sprint Mobile' => 'Sprint Mobile',
                )
            ])
            ->add('date_deb',DateType::class,
                [
                    'label' => 'Date Début',
                    'required' => true,
                    'widget' => 'single_text'
                ])
            ->add('date_fin',DateType::class,
                [
                    'label' => 'Date Fin',
                    'required' => true,
                    'widget' => 'single_text'
                ])
            ->add('specialite')
            ->add('coefSprint',NumberType::class,[
                'label' => 'Coefficient sprint'
            ])
            ->add('coefNoteIndiv',NumberType::class,[
                'label' => 'Coefficient note individuelle'
            ])
            ->add('coefNoteGroupe',NumberType::class,[
                'label' => 'Coefficient note de groupe'
            ])
            ->add('description',TextareaType::class, array(
                    'attr'=> array(
                        'rows' => '15'
                    )
                )
            );
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Sprint'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_sprint';
    }


}
