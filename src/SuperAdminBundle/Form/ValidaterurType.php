<?php

namespace SuperAdminBundle\Form;

use AppBundle\Repository\ClasseRepository;
use AppBundle\Repository\GroupeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ValidaterurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',TextType::class,array('label'=>'Prénom'))
            ->add('lastName',TextType::class,array('label'=>'Nom'))
            ->add('email',EmailType::class)
            ->add('username',TextType::class,array('label'=>'Nom d\'utilisateur',
                'attr'=>array(
                    'minlength'  => '4',
                )))
            ->add('plainPassword',TextType::class,
                array('label'=>'Mot de passe'
                ,'attr'=>array(
                    'pattern'  => '^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$',
                    'title'    => 'Veuillez inclure au moins 1 caractère majuscule, 1 caractère minuscule et 1 chiffre.'
                )))
            ->add('classe',EntityType::class,[
                'class' => 'AppBundle\Entity\Classe',
                'placeholder' => 'Selectionnez un Classe',
                'query_builder' => function(ClasseRepository $repo) {
                    return $repo->findWithGroupes();
                },
                'mapped' => false,
            ]);
            $builder->get('classe')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event){
                    $form = $event->getForm();
                    $form->getParent()->add('group',EntityType::class,[
                        'class' => 'AppBundle\Entity\Groupe',
                        'placeholder' => 'Selectionnez un groupe',
                        'mapped' => false,
                        'required' => true,
                        'query_builder' => function(GroupeRepository $repo) use ($form) {
                            return $repo->findWithSujet($form->getParent()->getData()->getClasse());
                        },
                    ]);
                }
            );
    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function getBlockPrefix()
    {
        return 'super_admin_bundle_validaterur_type';
    }
}
