<?php

namespace SuperAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdministrateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',TextType::class,array('label'=>'Prénom'))
            ->add('lastName',TextType::class,array('label'=>'Nom'))
            ->add('email',EmailType::class)
            ->add('username',TextType::class,array('label'=>'Nom d\'utilisateur',
                'attr'=>array(
                    'minlength'  => '4',
                )))
            ->add('plainPassword',TextType::class,
                array('label'=>'Mot de passe'
                ,'attr'=>array(
                    'pattern'  => '^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$',
                    'title'    => 'Veuillez inclure au moins 1 caractère majuscule, 1 caractère minuscule et 1 chiffre.'
                )));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function getBlockPrefix()
    {
        return 'super_admin_bundle_administrateur_type';
    }
}
