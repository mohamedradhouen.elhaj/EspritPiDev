<?php

namespace SuperAdminBundle\Controller;

use AppBundle\Entity\Sprint;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Sprint controller.
 *
 * @Route("sprint")
 */
class SprintController extends Controller
{
    /**
     * Lists all sprint entities.
     *
     * @Route("/", name="sprint_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sprints = $em->getRepository('AppBundle:Sprint')->findAll();

        return $this->render('SuperAdminBundle:sprint:index.html.twig', array(
            'sprints' => $sprints,
        ));
    }

    /**
     * Creates a new sprint entity.
     *
     * @Route("/new", name="sprint_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sprint = new Sprint();
        $form = $this->createForm('SuperAdminBundle\Form\SprintType', $sprint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sprint);
            $em->flush();

            return $this->redirectToRoute('sprint_index');
        }

        return $this->render('SuperAdminBundle:sprint:new.html.twig', array(
            'sprint' => $sprint,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sprint entity.
     *
     * @Route("/{id}", name="sprint_show")
     * @Method("GET")
     */
    public function showAction(Sprint $sprint)
    {
        $deleteForm = $this->createDeleteForm($sprint);

        return $this->render('@SuperAdmin/sprint/show.html.twig', array(
            'sprint' => $sprint,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Finds and displays a sprint entity.
     *
     * @Route("/delete/{id}", name="sprint_delete_get")
     * @Method("GET")
     */
    public function suprimerAction(Sprint $sprint)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($sprint);
        $em->flush();
        return $this->redirectToRoute('sprint_index');
    }

    /**
     * Displays a form to edit an existing sprint entity.
     *
     * @Route("/{id}/edit", name="sprint_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Sprint $sprint)
    {
        $deleteForm = $this->createDeleteForm($sprint);
        $editForm = $this->createForm('SuperAdminBundle\Form\SprintType', $sprint);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sprint_index', array('id' => $sprint->getId()));
        }

        return $this->render('@SuperAdmin/sprint/edit.html.twig', array(
            'sprint' => $sprint,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sprint entity.
     *
     * @Route("/{id}", name="sprint_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Sprint $sprint)
    {
        $form = $this->createDeleteForm($sprint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sprint);
            $em->flush();
        }

        return $this->redirectToRoute('sprint_index');
    }

    /**
     * Creates a form to delete a sprint entity.
     *
     * @param Sprint $sprint The sprint entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sprint $sprint)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sprint_delete', array('id' => $sprint->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
