<?php

namespace SuperAdminBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User controller.
 *
 * @Route("tuteur")
 */
class GestionTuteurController extends Controller
{
    /**
     *
     * @Route(path="/",name="su_tuteur_list")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findByRole("ROLE_TUTEUR");

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $users, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );

        return $this->render('@SuperAdmin/Tuteur/index.html.twig', array('users' => $pagination));

    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="su_tuteur_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('SuperAdminBundle\Form\UserType', $user);
        $form->add('classe');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $user->addRole("ROLE_TUTEUR");
            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('su_tuteur_list');
        }

        return $this->render('@SuperAdmin/Tuteur/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="su_tuteur_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm('SuperAdminBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tuteur_edit', array('id' => $user->getId()));
        }

        return $this->render('@SuperAdmin/Tuteur/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
        ));
    }
    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="su_tuteur_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {

        return $this->render('@SuperAdmin/Tuteur/show.html.twig', array(
            'user' => $user,
        ));
    }


    /**
     * Deletes a user entity.
     *
     * @Route("/delete/{id}", name="su_tuteur_delete_get")
     * @Method("GET")
     */
    public function SupprimerAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('su_tuteur_list');
    }

    /**
     * Activate a user.
     *
     * @Route("/activate/{id}", name="tuteur_activate")
     * @Method("GET")
     */
    public function ActivateAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setEnabled(true);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('su_tuteur_list');
    }
    /**
     * Deactivate a user .
     *
     * @Route("/deactivate/{id}", name="tuteur_deactivate")
     * @Method("GET")
     */
    public function DeactivateAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setEnabled(false);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('su_tuteur_list');
    }
}
