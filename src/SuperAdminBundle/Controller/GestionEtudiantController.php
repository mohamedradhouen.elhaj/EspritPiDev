<?php

namespace SuperAdminBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User controller.
 *
 * @Route("Students")
 */
class GestionEtudiantController extends Controller
{
    /**
     *
     * @Route(path="/",name="StudentsList")
     * @Method({"GET"})
     */
    public function  indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $list = $em->getRepository('AppBundle:User')->findByRoleQuery('ROLE_ETUDIANT');
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $list, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 9)/*page number*/
        );

        return $this->render('@SuperAdmin/Etudiants/index.html.twig', array('users' => $pagination));

    }
    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="student_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('SuperAdminBundle\Form\EtudiantType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $user->setEnabled(true);
            $user->addRole('ROLE_ETUDIANT');
            $user->setPlainPassword($user->getUsername());
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('StudentsList');
        }

        return $this->render('@SuperAdmin/Etudiants/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }
    /**
     * Creates a new user entity.
     *
     * @Route("/etudiant/import", name="students_import")
     * @Method({"GET", "POST"})
     */
    public function ImporterEtudiantsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        if(isset($_POST["Import"])){

            $filename=$_FILES["file"]["tmp_name"];


            if($_FILES["file"]["size"] > 0)
            {
                $file = fopen($filename, "r");

                while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                {
                    $user = new User();
                    $user->setFirstName($getData[12]);
                    $user->setLastName($getData[13]);
                    $user->setEmail($getData[4]);
                    $user->setEmailCanonical($getData[4]);
                    $user->setUsername($getData[3]);
                    $user->setUsernameCanonical($getData[3]);
                    $user->setPlainPassword($getData[3]);
                    $user->addRole('ROLE_ETUDIANT');
                    $user->setEnabled(true);
                    $testUser = $em->getRepository('AppBundle:User')->findBy(['email' => $getData[4]]);
                    if ($testUser == null){
                        $em->persist($user);
                        $em->flush();
                    }


                }

                fclose($file);

                return $this->redirectToRoute('StudentsList');
            }else{
                return  $this->redirectToRoute('StudentsList');
            }
        }

        return $this->redirectToRoute('StudentsList');


    }


    /**
     *
     * @Route("/deleteStudents", name="delete_students")
     * @Method({"GET", "POST"})
     */
    public function DeleteEtudiantsAction(Request $request)
    {

        $em=$this->getDoctrine()->getManager();
        if($request->isMethod('get'))
        {
            $list=$request->get('Etudiants');

            if (empty($list)){
                return $this->redirectToRoute('StudentsList');
            } else{

                foreach($list as $v => $value) {
                    $emp=$em->getRepository('AppBundle:User')->find($value);
                    $em->remove($emp);
                    $em->flush();
                    $em->clear();

                }
                return $this->redirectToRoute('StudentsList');
            }

        }
    }
    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="student_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm('SuperAdminBundle\Form\EtudiantType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('StudentsList', array('id' => $user->getId()));
        }

        return $this->render('@SuperAdmin/Etudiants/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
        ));
    }


    /**
     * Deletes a user entity.
     *
     * @Route("/delete/{id}", name="delete_student")
     * @Method("GET")
     */
    public function SupprimerAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('StudentsList');
    }
    /**
     * Activate a user.
     *
     * @Route("/activate/{id}", name="student_activate")
     * @Method("GET")
     */
    public function ActivateAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setEnabled(true);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('StudentsList');
    }
    /**
     * Deactivate a user .
     *
     * @Route("/deactivate/{id}", name="student_deactivate")
     * @Method("GET")
     */
    public function DeactivateAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setEnabled(false);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('StudentsList');
    }
}
