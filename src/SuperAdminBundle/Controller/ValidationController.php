<?php

namespace SuperAdminBundle\Controller;

use AppBundle\Entity\Groupe;
use AppBundle\Entity\Validation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Validation controller.
 *
 * @Route("validation")
 */
class ValidationController extends Controller
{
    /**
     * Lists all validation entities.
     * @Route("/", name="validation_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $validations = $em->getRepository('AppBundle:Validation')->findAll();
        $groupes = $em->getRepository('AppBundle:Groupe')->findAll();
        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $validations, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );
        $paginationGroupes = $paginator->paginate(
            $groupes, /* query NOT result */
            $request->query->getInt('pageGroupe', 1)/*page number*/,
            5,/*page number*/
            ['pageParameterName' => 'pageGroupe']
        );
        return $this->render('SuperAdminBundle:Validation:index.html.twig', array(
            'validations' => $pagination,
            'groupes' => $paginationGroupes,
        ));
    }

    /**
     * Creates a new validation entity.
     *
     * @Route("/new/{groupe}", name="validation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request , Groupe $groupe)
    {
        $validation = new Validation();
        $validation->setGroupe($groupe);
        $validation->setSpecialite($groupe->getClasse()->getSpecialite());
        $form = $this->createForm('SuperAdminBundle\Form\ValidationType', $validation);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            dump($validation);
            $em->persist($validation);
            $em->flush();

            return $this->redirectToRoute('validation_index');
        }

        return $this->render('@SuperAdmin/validation/new.html.twig', array(
            'validation' => $validation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a validation entity.
     *
     * @Route("/{id}", name="validation_show")
     * @Method("GET")
     */
    public function showAction(Validation $validation)
    {
        $deleteForm = $this->createDeleteForm($validation);

        return $this->render('@SuperAdmin/validation/show.html.twig', array(
            'validation' => $validation,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Finds and displays a validation entity.
     *
     * @Route("/delete/{id}", name="validation_delete_get")
     * @Method("GET")
     */
    public function SupprimerAction(Validation $validation)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($validation);
        $em->flush();
        return $this->redirectToRoute('validation_index');
    }
    /**
     * Displays a form to edit an existing validation entity.
     *
     * @Route("/{id}/edit", name="validation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Validation $validation)
    {
        $deleteForm = $this->createDeleteForm($validation);
        $editForm = $this->createForm('SuperAdminBundle\Form\ValidationType', $validation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('validation_edit', array('id' => $validation->getId()));
        }

        return $this->render('@SuperAdmin/validation/edit.html.twig', array(
            'validation' => $validation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a validation entity.
     *
     * @Route("/{id}", name="validation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Validation $validation)
    {
        $form = $this->createDeleteForm($validation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($validation);
            $em->flush();
        }

        return $this->redirectToRoute('validation_index');
    }

    /**
     * Creates a form to delete a validation entity.
     *
     * @param Validation $validation The validation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Validation $validation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('validation_delete', array('id' => $validation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
