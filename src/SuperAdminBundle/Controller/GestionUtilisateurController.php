<?php

namespace SuperAdminBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User controller.
 *
 * @Route("Users")
 */
class GestionUtilisateurController extends Controller
{
    /**
     *
     * @Route(path="/",name="userList")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $list = $em->getRepository('AppBundle:User')->findallUserseQuery();
        $users = [];
        foreach ($list as $value){
            if ($value->hasRole("ROLE_SUPER_ADMIN") or  $value->hasRole("ROLE_ADMIN")){

            }else{
                array_push($users,$value);
            }
        }
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $list, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 9)/*page number*/
        );

        return $this->render('@SuperAdmin/Users/index.html.twig', array('users' => $pagination));

    }
    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('SuperAdminBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('userList');
        }

        return $this->render('@SuperAdmin/Users/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm('SuperAdminBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('userList', array('id' => $user->getId()));
        }

        return $this->render('@SuperAdmin/Users/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
        ));
    }


    /**
     * Deletes a user entity.
     *
     * @Route("/delete/{id}", name="delete_user")
     * @Method("GET")
     */
    public function SupprimerAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('userList');
    }
    /**
     * Activate a user.
     *
     * @Route("/activate/{id}", name="user_activate")
     * @Method("GET")
     */
    public function ActivateAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setEnabled(true);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('userList');
    }
    /**
     * Deactivate a user .
     *
     * @Route("/deactivate/{id}", name="user_deactivate")
     * @Method("GET")
     */
    public function DeactivateAction(Request $request, User $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setEnabled(false);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('userList');
    }
}
