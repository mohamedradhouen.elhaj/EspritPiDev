<?php

namespace SuperAdminBundle\Controller;

use AppBundle\Entity\Sujet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Sujet controller.
 *
 * @Route("sujet")
 */
class SujetController extends Controller
{
    /**
     * Lists all sujet entities.
     *
     * @Route("/", name="su_sujet_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sujets = $em->getRepository('AppBundle:Sujet')->findAll();

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $sujets, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );
        return $this->render('@SuperAdmin/sujet/index.html.twig', array(
            'sujets' => $pagination,
        ));
    }

    /**
     * Creates a new sujet entity.
     *
     * @Route("/new", name="su_sujet_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sujet = new Sujet();
        $form = $this->createForm('SuperAdminBundle\Form\SujetType', $sujet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sujet);
            $em->flush();

            return $this->redirectToRoute('su_sujet_index');
        }

        return $this->render('SuperAdminBundle:sujet:new.html.twig', array(
            'sujet' => $sujet,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sujet entity.
     *
     * @Route("/{id}", name="su_sujet_show")
     * @Method("GET")
     */
    public function showAction(Sujet $sujet)
    {

        return $this->render('@SuperAdmin/sujet/show.html.twig', array(
            'sujet' => $sujet
        ));
    }

    /**
     * Displays a form to edit an existing sujet entity.
     *
     * @Route("/{id}/edit", name="su_sujet_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Sujet $sujet)
    {

        $editForm = $this->createForm('SuperAdminBundle\Form\SujetType', $sujet);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('su_sujet_index', array('id' => $sujet->getId()));
        }

        return $this->render('@SuperAdmin/sujet/edit.html.twig', array(
            'sujet' => $sujet,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Deletes a specialite entity.
     *
     * @Route("/delete/{id}", name="su_sujet_delete")
     * @Method("GET")
     */
    public function SuprimerAction(Request $request, Sujet $id)
    {

        $em = $this->getDoctrine()->getManager();
        $specialite = $em->getRepository('AppBundle:Sujet')->find($id);
        $em->remove($specialite);
        $em->flush();

        return $this->redirectToRoute('su_sujet_index');
    }
    /**
     * Deletes a sujet entity.
     *
     * @Route("/{id}", name="sujet_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Sujet $sujet)
    {
        $form = $this->createDeleteForm($sujet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sujet);
            $em->flush();
        }

        return $this->redirectToRoute('su_sujet_index');
    }

    /**
     * Creates a form to delete a sujet entity.
     *
     * @param Sujet $sujet The sujet entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sujet $sujet)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sujet_delete', array('id' => $sujet->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
