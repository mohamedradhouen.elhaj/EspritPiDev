<?php

namespace SuperAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/",name="homepage_super_admin")
     */
    public function indexAction()
    {
        return $this->render('SuperAdminBundle:Default:index.html.twig');
    }
}
