<?php

namespace SuperAdminBundle\Controller;

use AppBundle\Entity\Classe;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Classe controller.
 *
 * @Route("classe")
 */
class ClasseController extends Controller
{
    /**
     * Lists all classe entities.
     *
     * @Route("/", name="su_classe_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $classe = new Classe();
        $form = $this->createForm('SuperAdminBundle\Form\ClasseType', $classe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($classe);
            $em->flush();

            return $this->redirectToRoute('su_classe_index');
        }

        $classes = $em->getRepository('AppBundle:Classe')->findAll();

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $classes, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 9)/*page number*/
        );
        return $this->render('@SuperAdmin/classe/index.html.twig', array(
            'classes' => $pagination,
            'classe' => $classe,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new classe entity.
     *
     * @Route("/new", name="su_classe_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $classe = new Classe();
        $form = $this->createForm('SuperAdminBundle\Form\ClasseType', $classe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($classe);
            $em->flush();

            return $this->redirectToRoute('su_classe_show', array('id' => $classe->getId()));
        }

        return $this->render('@SuperAdmin/classe/new.html.twig', array(
            'classe' => $classe,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a classe entity.
     *
     * @Route("/{id}", name="su_classe_show")
     * @Method("GET")
     */
    public function showAction(Request $request ,Classe $classe)
    {
        $editForm = $this->createForm('SuperAdminBundle\Form\ClasseType', $classe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('su_classe_edit', array('id' => $classe->getId()));
        }

        $paginator  = $this->get('knp_paginator');

        $paginationEtudiant = $paginator->paginate(
            $classe->getEtudiants(), /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 3)/*page number*/
        );
        $paginationTuteur = $paginator->paginate(
            $classe->getTuteurs(), /* query NOT result */
            $request->query->getInt('pageTuteur', 1)/*page number*/,
            3,/*page number*/
            ['pageParameterName' => 'pageTuteur']
        );
        $paginationGroupes = $paginator->paginate(
            $classe->getGroupes(), /* query NOT result */
            $request->query->getInt('pageGroupe', 1)/*page number*/,
            3,/*page number*/
            ['pageParameterName' => 'pageGroupe']
        );
        return $this->render('@SuperAdmin/classe/show.html.twig', array(
            'classe' => $classe,
            'tuteurs' => $paginationTuteur,
            'etudiants' => $paginationEtudiant,
            'groupes'=> $paginationGroupes,
            'edit_form' => $editForm->createView(),
        ));
    }
    /**
     * Finds and displays a classe entity.
     *
     * @Route("/tuteurs/{id}", name="su_classe_tuteurs_show")
     * @Method("GET")
     */
    public function showTuteursAction(Request $request ,Classe $classe)
    {
        $em = $this->getDoctrine()->getManager();
        $paginator  = $this->get('knp_paginator');
        $users =  $em->getRepository('AppBundle:User')->findEnabledUsersByRole("ROLE_TUTEUR");
        $paginationTuteur = $paginator->paginate(
            $users, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 9)/*page number*/
        );

        return $this->render('@SuperAdmin/classe/addTuteurs.html.twig', array(
            'classe' => $classe,
            'tuteurs' => $paginationTuteur,
        ));
    }
    /**
     * Finds and displays a classe entity.
     *
     * @Route("/etudiants/{id}", name="su_classe_etudinats_show")
     * @Method("GET")
     */
    public function showStudentsAction(Request $request ,Classe $classe)
    {
        $em = $this->getDoctrine()->getManager();
        $paginator  = $this->get('knp_paginator');
        $users =  $em->getRepository('AppBundle:User')->findByStudentswithoutClasses("ROLE_ETUDIANT");
        $paginationTuteur = $paginator->paginate(
            $users, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 9)/*page number*/
        );

        return $this->render('@SuperAdmin/classe/addStudents.html.twig', array(
            'classe' => $classe,
            'etudinats' => $paginationTuteur,
        ));
    }
    /**
     * Finds and displays a classe entity.
     *
     * @Route("addTuteur/{id}", name="su_classe_add_tuteurs")
     * @Method("GET")
     */
    public function AddTuteurAction(Request $request,Classe $classe)
    {
        $list=array();
        $em=$this->getDoctrine()->getManager();
        if($request->isMethod('post'))
        {
            $list=$request->get('Tuteurs');
            if (empty($list)){
                return $this->redirectToRoute('su_classe_tuteurs_show',array('id'=>$classe->getId()));
            }else{
                foreach($list as $v => $value) {
                    $emp=$em->getRepository('AppBundle:User')->find($value);
                    $emp->addTutuerClasse($classe);
                    $em->persist($emp);
                    $em->flush();
                }
                return $this->redirectToRoute('su_classe_show',array('id'=>$classe->getId()));
            }


        }


        //return $this->render('@Projet/test.html.twig',array('test'=>$list,'id'=>$id));
    }

    /**
     * Finds and displays a classe entity.
     *
     * @Route("addEtudiants/{id}", name="su_classe_add_students")
     * @Method("GET")
     */
    public function AddEtudiantsAction(Request $request,Classe $classe)
    {
        $list=array();
        $em=$this->getDoctrine()->getManager();
        if($request->isMethod('post'))
        {
            $list=$request->get('Etudinats');
            if (empty($list)){
                return $this->redirectToRoute('su_classe_tuteurs_show',array('id'=>$classe->getId()));
            }else{
                foreach($list as $v => $value) {
                    $emp=$em->getRepository('AppBundle:User')->find($value);
                    $emp->setClasse($classe);
                    $em->persist($emp);
                    $em->flush();
                }
                return $this->redirectToRoute('su_classe_show',array('id'=>$classe->getId()));
            }


        }


        //return $this->render('@Projet/test.html.twig',array('test'=>$list,'id'=>$id));
    }
    /**
     * Displays a form to edit an existing classe entity.
     *
     * @Route("/{id}/edit", name="su_classe_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Classe $classe)
    {
        $deleteForm = $this->createDeleteForm($classe);
        $editForm = $this->createForm('SuperAdminBundle\Form\ClasseType', $classe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('su_classe_edit', array('id' => $classe->getId()));
        }

        return $this->render('@SuperAdmin/classe/edit.html.twig', array(
            'classe' => $classe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a specialite entity.
     *
     * @Route("/delete/{id}", name="su_classe_delete")
     * @Method("GET")
     */
    public function SuprimerAction(Request $request, Classe $id)
    {

        $em = $this->getDoctrine()->getManager();
        $specialite = $em->getRepository('AppBundle:Classe')->find($id);
        $em->remove($specialite);
        $em->flush();

        return $this->redirectToRoute('su_classe_index');
    }
    /**
     * Deletes a tuteur from a classe entity.
     *
     * @Route("/delete_Tutueur/{classe}/{user}", name="su_classe_delete_tuteur")
     * @Method("GET")
     */
    public function deleteTuteurrAction(Request $request, Classe $classe,User $user)
    {

        $em = $this->getDoctrine()->getManager();
        $classe->removeUser($user);
        $em->persist($classe);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('su_classe_show',array('id'=>$classe->getId()));
    }
    /**
     * Deletes a tuteur from a classe entity.
     *
     * @Route("/delete_etudiant/{classe}/{user}", name="su_classe_delete_student")
     * @Method("GET")
     */
    public function deleteStudentAction(Request $request, Classe $classe,User $user)
    {

        $em = $this->getDoctrine()->getManager();
        $user->setClasse(null);
        $em->persist($classe);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('su_classe_show',array('id'=>$classe->getId()));
    }
    /**
     * Deletes a classe entity.
     *
     * @Route("/{id}", name="classe_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Classe $classe)
    {
        $form = $this->createDeleteForm($classe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($classe);
            $em->flush();
        }

        return $this->redirectToRoute('su_classe_index');
    }

    /**
     * Creates a form to delete a classe entity.
     *
     * @param Classe $classe The classe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Classe $classe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('classe_delete', array('id' => $classe->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
