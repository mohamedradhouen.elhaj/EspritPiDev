<?php

namespace SuperAdminBundle\Controller;

use AppBundle\Entity\Specialite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Specialite controller.
 *
 * @Route("specialite")
 */
class SpecialiteController extends Controller
{
    /**
     * Lists all specialite entities.
     *
     * @Route("/", name="su_specialite_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $specialites = $em->getRepository('AppBundle:Specialite')->findAll();
        $specialite = new Specialite();
        $form = $this->createForm('SuperAdminBundle\Form\SpecialiteType', $specialite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($specialite);
            $em->flush();

            return $this->redirectToRoute('su_specialite_index');
        }

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $specialites, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 9)/*page number*/
        );
        return $this->render('SuperAdminBundle:specialite:index.html.twig', array(
            'specialites' => $pagination,
            'specialite' => $specialite,
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a new specialite entity.
     *
     * @Route("/new", name="su_specialite_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $specialite = new Specialite();
        $form = $this->createForm('SuperAdminBundle\Form\SpecialiteType', $specialite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($specialite);
            $em->flush();

            return $this->redirectToRoute('su_specialite_index');
        }

        return $this->render('@SuperAdmin/specialite/new.html.twig', array(
            'specialite' => $specialite,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a specialite entity.
     *
     * @Route("/{id}", name="su_specialite_show")
     * @Method("GET")
     */
    public function showAction(Request $request,Specialite $specialite)
    {
        $editForm = $this->createForm('SuperAdminBundle\Form\SpecialiteType', $specialite);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('su_specialite_show', array('id' => $specialite->getId()));
        }

        $paginator  = $this->get('knp_paginator');

        $paginationClasse = $paginator->paginate(
            $specialite->getClasses(), /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );
        $paginationGroupes = $paginator->paginate(
            $specialite->getGroupes(), /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );
        return $this->render('@SuperAdmin/specialite/show.html.twig', array(
            'specialite' => $specialite,
            'classes' => $paginationClasse,
            'groupes'=> $paginationGroupes,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing specialite entity.
     *
     * @Route("/{id}/edit", name="su_specialite_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Specialite $specialite)
    {
        $deleteForm = $this->createDeleteForm($specialite);
        $editForm = $this->createForm('SuperAdminBundle\Form\SpecialiteType', $specialite);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('su_specialite_index', array('id' => $specialite->getId()));
        }

        return $this->render('@SuperAdmin/specialite/edit.html.twig', array(
            'specialite' => $specialite,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a specialite entity.
     *
     * @Route("/delete/{id}", name="su_specialite_delete")
     * @Method("GET")
     */
    public function SuprimerAction(Request $request, Specialite $id)
    {

        $em = $this->getDoctrine()->getManager();
        $specialite = $em->getRepository('AppBundle:Specialite')->find($id);
        $em->remove($specialite);
        $em->flush();

        return $this->redirectToRoute('su_specialite_index');
    }

    /**
     * Deletes a specialite entity.
     *
     * @Route("/{id}", name="specialite_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Specialite $specialite)
    {
        $form = $this->createDeleteForm($specialite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($specialite);
            $em->flush();
        }

        return $this->redirectToRoute('su_specialite_index');
    }

    /**
     * Creates a form to delete a specialite entity.
     *
     * @param Specialite $specialite The specialite entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Specialite $specialite)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('specialite_delete', array('id' => $specialite->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
