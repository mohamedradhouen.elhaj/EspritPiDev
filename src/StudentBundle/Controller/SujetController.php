<?php

namespace StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SujetController extends Controller
{
    /**
     * @Route("/SujetList", name="sujet_list")
     */
    public function listSujetAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sujets = $em->getRepository('AppBundle:Sujet')->findAll();

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $sujets, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );
        return $this->render('StudentBundle::sujetList.html.twig',array(
            'sujets' => $pagination
        ));
    }

    /**
     * @Route("/Validations", name="mes_validation")
     */
    public function listvalidationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sujets = $em->getRepository('AppBundle:Sujet')->findAll();

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $sujets, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*page number*/
        );
        return $this->render('StudentBundle::sujetList.html.twig',array(
            'sujets' => $pagination
        ));
    }
}
