<?php

namespace StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class NoteController extends Controller
{
    /**
     * @Route("/Notes", name="mes_notes")
     */
    public function indexAction()
    {
        return $this->render('StudentBundle::notes.html.twig');
    }
}
